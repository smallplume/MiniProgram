// pages/profile/profile.js
import request from '../../service/network.js'

var app = getApp();

Page({
  data: {
    userInfo: '',
    userTabs: [{
      icon: '/assets/common/payment.png',
      title: '待付款',
      url: ''
    }, {
      icon: '/assets/common/shipments.png',
      title: '待发货',
      url: ''
    }, {
      icon: '/assets/common/receiving.png',
      title: '待收货',
      url: ''
    }, {
      icon: '/assets/common/finash.png',
      title: '已完成',
      url: ''
    }, {
      icon: '/assets/common/pond.png',
      title: '售后服务',
      url: ''
    }],
    userActions: [{
      title: '我的优惠券',
      url: ''
    }, {
      title: '我的收藏',
      url: ''
    }, {
      title: '收货地址',
      url: ''
    }, {
      title: '我的资产',
      url: ''
    }, {
      title: '产品反馈',
      url: ''
    }]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // request({
    //   url: 'http://localhost:8080/app-service-sys/sys/login',
    //   header: app.globalData.header,
    //   method: 'post',
    //   data: {
    //     'code': app.globalData.code,
    //     'terminalId': 1
    //   }
    // }).then(res => {
    //   app.globalData.header = {
    //     'content-type': 'application/json',
    //     'token': res.data.data.token,
    //     'openId': res.data.data.openId,
    //     'terminalId': 1
    //   };
    //   console.log("res ================= " + res)
    // }).catch(err => {
    //   console.log(err)
    // })
  },

  // 检查用户登录状态
  checkUserStatus: function() {
    let userInfo = app.globalData.userInfo;
    if(userInfo == null) {
      // 跳转到登录页面
      wx.navigateTo({
        url: '/pages/login/login',
        success: function(res) {},
        fail: function(res) {},
        complete: function(res) {}
      })
    } else {
      // 跳转到详情页

    }
  },

  /**
   * 通过按钮点击
   */
  // getUserInfo: function() {
  //   // 登录
  //   wx.login({
  //     success: res => {
  //       // 发送 res.code 到后台换取 openId, sessionKey, unionId
  //       // 也就是发送到后端,后端通过接口发送到前端，前端接收用户信息等....
  //       wx.setStorageSync('code', res.code);
  //       console.log(wx.getStorageSync('code'))

  //       // 获取用户信息
  //       wx.getSetting({
  //         success: res => {
  //           if (res.authSetting['scope.userInfo']) {
  //             // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
  //             wx.getUserInfo({
  //               success: res => {
  //                 // 可以将 res 发送给后台解码出 unionId
  //                 app.globalData.userInfo = res.userInfo

  //                 // // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
  //                 // // 所以此处加入 callback 以防止这种情况
  //                 if (this.userInfoReadyCallback) {
  //                   this.userInfoReadyCallback(res)
  //                 }
  //               }
  //             })
  //           }
  //         }
  //       })
  //     }
  //   })
  // },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})
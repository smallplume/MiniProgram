// pages/home/home.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperImages: [
      '/assets/home/swiper01.png',
      '/assets/home/swiper02.png',
      '/assets/home/swiper03.png',
      '/assets/home/swiper04.png'
    ],
    // 是否显示面板指示点
    indicatorDots: true,
    // 指示点颜色
    indicatorColor: 'gray',
    // 选中的指示颜色
    indicatorActiveColor: 'red',
    // 自动切换
    autoplay: true,
    // 切换时间
    interval: 3000,
    // 滑动时长
    duration: 1000,
    // 是否采用衔接滑动
    circular: false,

    currentTab: 0,

    recommendData: [{
        title: '十点抢券',
        imgUrl: '/assets/home/block01.png'
      },
      {
        title: '好物特卖',
        imgUrl: '/assets/home/block02.png'
      },
      {
        title: '内购福利',
        imgUrl: '/assets/home/block03.png'
      },
      {
        title: '初秋上新',
        imgUrl: '/assets/home/block04.png'
      }
    ],
    fashionData: [{
        title: '霸街长袖裙',
        imgUrl: '/assets/home/fashion01.png'
      },
      {
        title: '时尚懒人套装',
        imgUrl: '/assets/home/fashion02.png'
      },
      {
        title: '微胖遮肉穿搭',
        imgUrl: '/assets/home/fashion03.png'
      },
      {
        title: '降温保暖鞋',
        imgUrl: '/assets/home/fashion04.png'
      },
      {
        title: '颜值爆表套装裙',
        imgUrl: '/assets/home/fashion05.png'
      },
      {
        title: '小心机v领',
        imgUrl: '/assets/home/fashion06.png'
      },
      {
        title: '万圣节必备口红',
        imgUrl: '/assets/home/fashion07.png'
      },
      {
        title: '软萌暖心包包',
        imgUrl: '/assets/home/fashion08.png'
      }
    ],
    classifyData: [
      '流行', '新款', '精选'
    ],
    classifyList: [],
    classifyList0: [{
        title: '流行最前线',
        evaluate: '上身很好看哒。哈哈哈哈。赚到～162/50的，我上身瘦，穿的xs，合适，不过不能套很厚的毛衣。',
        url: '/assets/home/classify01.png'
      },
      {
        title: '今年的潮款搭配',
        evaluate: '第一次这么急着给评价，总体挺好的 发货快，物流快 衣服质量和实体店没有多大差距 衣服很好看，喜欢😍',
        url: '/assets/home/classify02.png'
      },
      {
        title: '九月份流行穿着',
        evaluate: '很好看！确实也是棉的~不是厚的那种也不会太薄，很舒适！款式和颜色都跟模特图无误~推荐购买',
        url: '/assets/home/classify03.png'
      }
    ],
    classifyList1: [{
        title: '流行最前线',
        evaluate: '上身很好看哒。哈哈哈哈。赚到～162/50的，我上身瘦，穿的xs，合适，不过不能套很厚的毛衣。',
        url: '/assets/home/classify04.png'
      },
      {
        title: '今年的潮款搭配',
        evaluate: '第一次这么急着给评价，总体挺好的 发货快，物流快 衣服质量和实体店没有多大差距 衣服很好看，喜欢😍',
        url: '/assets/home/classify05.png'
      },
      {
        title: '九月份流行穿着',
        evaluate: '很好看！确实也是棉的~不是厚的那种也不会太薄，很舒适！款式和颜色都跟模特图无误~推荐购买',
        url: '/assets/home/classify06.png'
      }
    ],
    classifyList2: [{
        title: '流行最前线',
        evaluate: '上身很好看哒。哈哈哈哈。赚到～162/50的，我上身瘦，穿的xs，合适，不过不能套很厚的毛衣。',
        url: '/assets/home/classify07.png'
      },
      {
        title: '今年的潮款搭配',
        evaluate: '第一次这么急着给评价，总体挺好的 发货快，物流快 衣服质量和实体店没有多大差距 衣服很好看，喜欢😍',
        url: '/assets/home/classify08.png'
      },
      {
        title: '九月份流行穿着',
        evaluate: '很好看！确实也是棉的~不是厚的那种也不会太薄，很舒适！款式和颜色都跟模特图无误~推荐购买',
        url: '/assets/home/classify09.png'
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.selectList(0);
    // const appInstance = getApp()
    // console.log("getApp ====== " + appInstance.globalData.header)
  },
  clickTab: function(e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      this.selectList(e.target.dataset.current);
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },
  swiperChange: function(e) {
    var that = this;
    if ('touch' === e.detail.source) {
      let currentPageIndex = e.detail.current;
      this.selectList(currentPageIndex);
      that.setData({
        currentTab: currentPageIndex
      })
    }
  },
  selectList: function (index) {
    var that = this;
    let list = [];
    switch (index) {
      case 0:
        list = this.data.classifyList0;
        break;
      case 1:
        list = this.data.classifyList1;
        break;
      case 2:
        list = this.data.classifyList2;
        break;
    }
    that.setData({
      classifyList: list
    })
  }
})
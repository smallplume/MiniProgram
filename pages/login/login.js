// pages/login/login.js
import request from '../../service/network.js'

var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // var that = this;

    // //获取用户信息
    // wx.getUserInfo({
    //   success: function (res) {

    //     console.log(res);
    //     that.data.userInfo = res.userInfo;

    //     that.setData({
    //       userInfo: that.data.userInfo
    //     })
    //   }
    // })
  },

  /**
   * 微信登录
   */
  wxLogin: function() {
    wx.login({
      success: res => {
        var code = res.code;
        if (code) {
          // 保存缓存中
          wx.setStorageSync('code', res.code);
          // 登录后台
          request({
            url: 'http://localhost:8080/app-service-sys/sys/login',
            header: app.globalData.header,
            method: 'post',
            data: {
              'code': res.code,
              'terminalId': 1
            }
          }).then(res => {
            app.globalData.header = {
              'content-type': 'application/json',
              'token': res.data.data.token,
              'openId': res.data.data.openId,
              'terminalId': 1
            };
            app.globalData.userInfo = { 'avatarUrl': res.data.data.avatarUrl, 'nickName': res.data.data.nickName }
            wx.navigateBack({})
          }).catch(err => {
            console.log(err)
          })
        } else {
          console.log("用户微信登录失败：" + res.errMsg);
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})